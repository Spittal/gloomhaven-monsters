// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'whatwg-fetch';
import Vue from 'vue';
import App from './App';
import router from '@/core/routes';
import store from '@/core/store';

import localize from '@/core/shared/localization';
const browserLocale = navigator.browserLanguage || navigator.language || 'en-US';
const i18n = localize(browserLocale);

if (process.env.NODE_ENV !== 'development') {
  (function () {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/sw.js');
    }
  })();
}

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
});
