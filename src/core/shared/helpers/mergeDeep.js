export function isObject (item) {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

/**
 * Very similar to Object Assign except recursive and deep
 * @param  {Object} target
 * @param  {Object} source
 * @return {Object}        Merged Object
 */
export default function mergeDeep (target, source) {
  let output = Object.assign({}, target);
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!(key in target)) {
          Object.assign(output, { [key]: source[key] });
        } else {
          output[key] = mergeDeep(target[key], source[key]);
        }
      } else {
        Object.assign(output, { [key]: source[key] });
      }
    });
  }
  return output;
}
