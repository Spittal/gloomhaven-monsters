export default function handleClickOutside (selector, currentState, action) {
  const outsideClickListener = (event) => {
    if (!(event.target).closest(selector)) {
      action();
      removeClickListener();
    }
  };

  const removeClickListener = () => {
    document.removeEventListener('click', outsideClickListener);
  };

  if (!currentState) {
    // Set the time out so it doesn't apply to click in vue.
    setTimeout(() => {
      document.addEventListener('click', outsideClickListener);
    }, 0);
  }
}
