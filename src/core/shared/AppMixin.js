import Vue from 'vue';
import Component from 'vue-class-component';
import { mapState } from 'vuex';
import LoadingBar from '@/core/shared/components/LoadingBar';
import NetworkService from '@/core/shared/services/NetworkService';
import('normalize.css');

@Component({
  computed: mapState({
    flash: state => state.core.flash,
    loading: state => state.core.loading,
    navMenu: state => state.core.navMenu
  }),
  components: {
    'nav-menu': () => import('@/core/shared/components/NavMenu'),
    LoadingBar,
    'icons': () => import('@/core/shared/components/Icons'),
    'flash': () => import('@/core/shared/components/Flash')
  },
  watch: {
    '$route' () {
      this.closeNavMenu();
    }
  },
  created () {
    this.initApp();
  },
  methods: {
    initApp () {
      NetworkService.init({
        notify: message => this.$store.dispatch('flash', { message, type: 'info' }),
        onlineMessage: this.$t('Back online.'),
        offlineMessage: this.$t('Network changed, you are now offline.')
      });
    }
  },
  template: `
  <div id="app" :class="{ 'nav-menu-collapsed': navMenu.collapsed }">
    <icons />

    <nav-menu
      @closeNavMenu="closeNavMenu"
      @setCollapsed="setNavMenuCollapsed"

      :open="navMenu.open"
      :collapsed="navMenu.collapsed"

      :navigation="navigation" />

    <router-view />

    <flash :message="flash.message" :type="flash.type" :show="flash.show" />

    <loading-bar :loading="loading" />
  </div>
  `
})
export default class App extends Vue {
  get navigation () {
    return [];
  };

  burgerClick () {
    this.$store.commit('setNavMenuOpen', true);
  }

  closeNavMenu () {
    this.$store.commit('setNavMenuOpen', false);
  }

  setNavMenuCollapsed (value) {
    this.$store.commit('setNavMenuCollapsed', value);
  }
};
