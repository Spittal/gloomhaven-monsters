export default {
  loading: false,
  navMenu: {
    collapsed: false
  },
  flash: {
    type: 'success',
    message: '',
    show: false
  }
};
