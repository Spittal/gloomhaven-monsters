export const flash = ({ commit, state }, flash) => {
  commit('setFlash', flash);

  commit('setFlashShow', true);
  const timeout = (state.flash.message.length * 300 > 6000) ? state.flash.message.length * 300 : 6000;
  setTimeout(() => commit('setFlashShow', false), timeout);
};
