export const setLoading = (state, loading) => {
  state.loading = loading;
};

export const setNavMenuOpen = (state, open) => {
  state.navMenu.open = open;
};

export const setNavMenuCollapsed = (state, collapsed) => {
  state.navMenu.collapsed = collapsed;
};

export const setFlash = (state, { message, type }) => {
  state.flash.type = type;
  state.flash.message = message;
};

export const setFlashShow = (state, show) => {
  state.flash.show = show;
};
