import VueI18n from 'vue-i18n';
import Vue from 'vue';

import enUs from './en-US.json';

/**
 * An example of how different browsers report languages for english:
 *
 * Chrome/Firefox: 'en' or 'en-US'
 * Safari/IE: 'en' or 'en-us'
 *
 * Because of this we need to make sure that the index.js is handling all locale options
 */
export default (locale) => {
  Vue.use(VueI18n);

  const i18n = new VueI18n({
    locale,
    fallbackLocale: 'en-US',
    messages: {
      'en-US': enUs,
      'en-us': enUs,
      'en': enUs
    }
  });

  return i18n;
};
