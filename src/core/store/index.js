import Vue from 'vue';
import Vuex from 'vuex';
import * as getters from './getters';
import * as actions from './actions';
import * as mutations from './mutations';
import plugins from './plugins';
import { state } from './state';
import router from '../routes';
import { sync } from 'vuex-router-sync';

import core from '@/core/shared/store';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,
  state,
  getters,
  actions,
  mutations,
  plugins,
  modules: {
    core
  }
});

if (module.hot) {
  module.hot.accept([
    './getters',
    './actions',
    './mutations'
  ], () => {
    store.hotUpdate({
      getters: require('./getters'),
      actions: require('./actions'),
      mutations: require('./mutations'),
      modules: {
        core: require('@/core/shared/store')
      }
    });
  });
}

sync(store, router);

export default store;
