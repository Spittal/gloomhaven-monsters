FROM spittal/nginx-node

# Move package json over first for build speed improvements describes in link below
# http://jdlm.info/articles/2016/03/06/lessons-building-node-app-docker.html
WORKDIR /var/www/apps/gloomhavenmonsters.com/
COPY package.json yarn.lock /var/www/apps/gloomhavenmonsters.com/
RUN yarn

COPY .devops/docker-files/vhost.conf /etc/nginx/sites-enabled/gloomhavenmonsters.com.conf

COPY ./ /var/www/apps/gloomhavenmonsters.com/

CMD yarn build && nginx -g "daemon off;"
